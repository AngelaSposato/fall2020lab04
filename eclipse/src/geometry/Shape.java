//Angela Sposato 1934695
package geometry;

public interface Shape {
	double getArea();
	double getPerimeter();
}
