//Angela Sposato 1934695
package geometry;

public class Rectangle implements Shape {
	private double length;
	private double width;
	public Rectangle(double length, double width) {
		this.length = length;
		this.width = width;
	}
	public double getLength() {
		return length;
	}
	public double getWidth() {
		return width;
	}
	public double getPerimeter() {
		double perimeter = (2*length)+ (2*width);
		return perimeter;
	}
	public double getArea() {
		double area = length*width;
		return area;
	}
}
