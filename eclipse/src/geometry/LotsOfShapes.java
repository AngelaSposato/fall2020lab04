//Angela Sposato 1934695
package geometry;

public class LotsOfShapes {
	public static void main(String[] args) {
		Shape[] newShapes = new Shape[5];
		newShapes[0] = new Rectangle(3, 4);
		newShapes[1] = new Rectangle(4, 5);
		newShapes[2] = new Circle(3);
		newShapes[3] = new Circle(5);
		newShapes[4] = new Square(6);
		System.out.println("Perimeter of shapes:");
		for (int i = 0; i<newShapes.length; i++) {
			System.out.println(newShapes[i].getPerimeter());
			//System.out.println
		}
		System.out.println();
		System.out.println("Area of shapes:");
		for (int i = 0; i<newShapes.length; i++) {
			System.out.println(newShapes[i].getArea());
		}
	}
}
