//Angela Sposato 1934695
package geometry;

public class Circle implements Shape{
	private double radius;
	public Circle(double radius) {
		this.radius=radius;
	}
	public double getRadius() {
		return radius;
	}
	public double getPerimeter() {
		double circumference = 2*Math.PI*radius;
		return circumference;
	}
	public double getArea() {
		double area = Math.PI*(radius*radius);
		return area;
	}
}
