//Angela Sposato 1934695
package inheritance;

public class ElectronicBook extends Book {
	int numberBytes;
	public ElectronicBook(int numberBytes, String title, String author) {
		super(title, author);
		this.numberBytes = numberBytes;
	}
	public String toString() {
		String fromBase = super.toString();
		return fromBase + "number of bytes : " + numberBytes;
	}
}
