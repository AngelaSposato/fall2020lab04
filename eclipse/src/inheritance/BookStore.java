//Angela Sposato 1934695
package inheritance;

public class BookStore {

	public static void main(String[] args) {
		Book[] newBooks = new Book[5];
		newBooks[0]=new Book("Harry Potter", "JK Rowling");
		newBooks[1]=new ElectronicBook(17,"The Lion, The Witch, and the Wardrobe", "C. S Lewis");
		newBooks[2]=new Book("Alice's Adventures In Wonderland", "Lewis Caroll");
		newBooks[3]=new ElectronicBook(19, "The Hobbit", "J. R. R. Tolkien");
		newBooks[4]=new ElectronicBook(20, "A Wrinkle In Time", "Madeleine L'Engle");
		for (int i = 0; i<newBooks.length; i++) {
			System.out.println(newBooks[i]);
		}
	}

}
